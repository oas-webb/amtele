<?php
    get_header();
    $single_supplier_link = get_field('supplier_post_type');
    $single_supplier_link_output = $single_supplier_link['url'];

    
    $single_product = get_field('products');
    $specifications = $single_product['specifications'];
    $further = $single_product['further'];
	$contactForm = $single_product['form'];
    
    
    $lev_logo = $single_product['leverantor']['url'];
    $lev_logo_link = $single_product['leverantorslank'];

    $specific_supplier_link = $single_product['specific_supplier_link'];
	
?>
<div class="wrapper single-wrapper">
    <div class="inner single-inner">
        <div class="title-wrapper">
            <h1><?php the_title(); ?></h1>
        </div>
        <div class="col">
                <div class="lev-logo-wrapper">    
                    <h2><?php echo $single_product['leverator_rubrik']; ?></h2>



                    <?php

                        if($specific_supplier_link) { ?>
                            <a href="<?php echo $specific_supplier_link; ?>">
                                <?php if($single_product['leverantorer_relation']) {
                                    foreach( $single_product['leverantorer_relation'] as $relation_item ) {
                                        $relation_logo_src = get_field('supplier_post_type', $relation_item->ID);
                                        $relation_logo_url = get_field('supplier_post_type', $relation_item->ID);
                                        ?>
                                        <img class="supplier-logo-tax" src="<?php echo $relation_logo_src['image']['url']; ?>" alt="<?php echo $relation_logo_src['image']['title']; ?>">
                                    <?php }
                                } ?>
                            </a>
                        <?php }

                        if(!$specific_supplier_link) {
                            if($single_product['leverantorer_relation']) {
                                foreach( $single_product['leverantorer_relation'] as $relation_item ) {
                                    $relation_logo_src = get_field('supplier_post_type', $relation_item->ID);
                                    $relation_logo_url = get_field('supplier_post_type', $relation_item->ID);
                                    ?>
                                    <a href="<?php echo $relation_logo_src['url']; ?>">
                                        <img class="supplier-logo-tax" src="<?php echo $relation_logo_src['image']['url']; ?>" alt="<?php echo $relation_logo_src['image']['title']; ?>">
                                    </a>
                                <?php }
                            }
                        }
                    ?>

                    
                </div>
            <?php //endif; ?>
            <div class="excerpt-wrapper">
                <?php the_excerpt(); ?>
            </div>
            <div class="product-info-wrapper">
                <h4><?php echo $single_product['product_info_title']; ?></h4>
                <?php echo $single_product['product_info']; ?>
            </div>
            <?php if($single_product['embedded_youtube']): ?>
                <div class="iframe-wrapper">
                    <?php echo $single_product['embedded_youtube']; ?>
                </div>
            <?php endif; ?>
            <div class="specifications-wrapper">
                <h4><?php echo $single_product['product_specification_title']; ?></h4>
                <?php foreach( $specifications as $specification ): ?>
                    <div class="row">
                        <div class="col">
                            <?php echo $specification['title']; ?>
                        </div>
                        <div class="col">
                            <?php echo $specification['description']; ?>
                        </div>
                    </div>
                <?php endforeach; ?>
            </div>


            <?php if($single_product['further_title']): ?>
                <div class="further-wrapper">
                    <h4><?php echo $single_product['further_title']; ?></h4>
                    <?php foreach( $further as $further_item ): ?>
                        <div class="row">
                            <div class="col">
                                <a taget="<?php echo $further_item['link']['target']; ?>" href="<?php echo $further_item['link']['url']; ?>">
                                    <?php echo $further_item['link']['title']; ?>
                                </a>
                            </div>
                            <div class="col">
                                <?php //var_dump($further_item['download']); ?>
                                <?php if($further_item['download']['url']): ?>
                                    <a download href="<?php echo $further_item['download']['url']; ?>">
                                        <svg width="14" height="14" viewBox="0 0 64 64" fill="none" xmlns="http://www.w3.org/2000/svg">
                                            <path d="M50.27 30L32.27 48L14.27 30" stroke="#010101" stroke-width="4" stroke-miterlimit="10"/>
                                            <path d="M32.27 0V48" stroke="#010101" stroke-width="4" stroke-miterlimit="10"/>
                                            <path d="M14 61H50" stroke="#010101" stroke-width="4" stroke-miterlimit="10"/>
                                        </svg>
                                        <?php echo $further_item['download']['title']; ?>
                                    </a>
                                <?php endif; ?>
                            </div>
                        </div>
                    <?php endforeach; ?>
                </div>
            <?php endif; ?>
        </div>
        <div class="col post-thumbnail">
            <?php the_post_thumbnail(); ?>
        </div>
        <div class="single-contact-wrapper block">
            <!-- Hårdkodat -->
            <?php
                $contact_title = get_field('produkt','options');
            ?>
            <h2><?php echo $contact_title['produkt_kontakt_titel']; ?></h2>
            <?php
                $featured_posts = $single_product['contact_us'];
                if( $featured_posts ): ?>
                    <ul>
                        <?php foreach( $featured_posts as $post ):
                            setup_postdata($post); ?>
                            <li class="contact-item">
                                <?php the_post_thumbnail(); ?>
                                <div class="meta">
                                    <?php the_content(); ?>
                                </div>
                            </li>
                        <?php endforeach; ?>
                    </ul>
                    <?php
                    wp_reset_postdata(); ?>
                <?php endif; ?>
        </div>
		
		<div class="contact-form-wrapper" style="width:100%;display:flex;justify-content:center;align-items:center;height:auto;background:#eee;padding:5em 1.5em;border-radius: 25px 5px 25px 5px;margin-bottom:5em;order:5!important;">
			<?php echo $contactForm; ?>
		</div>

    </div>
</div>
<?php get_footer(); ?>