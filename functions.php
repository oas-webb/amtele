<?php

// FIX ACF IMAGE LIB
function acf_filter_rest_api_preload_paths( $preload_paths ) {
  if ( ! get_the_ID() ) {
    return $preload_paths;
  }
  $remove_path = '/wp/v2/' . get_post_type() . 's/' . get_the_ID() . '?context=edit';
  $v1 =  array_filter(
    $preload_paths,
    function( $url ) use ( $remove_path ) {
      return $url !== $remove_path;
    }
  );
  $remove_path = '/wp/v2/' . get_post_type() . 's/' . get_the_ID() . '/autosaves?context=edit';
  return array_filter(
    $v1,
    function( $url ) use ( $remove_path ) {
      return $url !== $remove_path;
    }
  );
}
add_filter( 'block_editor_rest_api_preload_paths', 'acf_filter_rest_api_preload_paths', 10, 1 );

// Order the posts Alphabetically


// show_admin_bar(false);

// Yoas quickfix count words
// add_filter('admin_enqueue_scripts', function() {
//   $yoast_acf_analysis_plugin_data = get_plugin_data( AC_SEO_ACF_ANALYSIS_PLUGIN_FILE );
//   $config = Yoast_ACF_Analysis_Facade::get_registry()->get( 'config' );

//   // Post page enqueue.
//   if ( wp_script_is( WPSEO_Admin_Asset_Manager::PREFIX . 'post-edit-classic' ) ) {
//       wp_enqueue_script(
//           'yoast-acf-analysis-post',
//           plugins_url( '/js/yoast-acf-analysis.js', AC_SEO_ACF_ANALYSIS_PLUGIN_FILE ),
//           [ 'jquery', WPSEO_Admin_Asset_Manager::PREFIX . 'post-edit-classic', 'underscore' ],
//           $yoast_acf_analysis_plugin_data['Version'],
//           true
//       );

//       wp_localize_script( 'yoast-acf-analysis-post', 'YoastACFAnalysisConfig', $config->to_array() );
//   }
// }, 20, 0);

// Add styles and scripts
function oas_scripts() {

 // CSS
 wp_enqueue_style ( 'oas_styles', get_template_directory_uri() . '/dev/styles.css' );

 // JS
 wp_enqueue_script ( 'oas_scripts_jquery', get_template_directory_uri() . '/dev/jquery.js' );
 wp_enqueue_script ( 'oas_scripts', get_template_directory_uri() . '/dev/scripts.js' );

 wp_localize_script('oas_scripts', 'wpAjax', array('ajaxUrl' => admin_url('admin-ajax.php')));

}
add_action('wp_enqueue_scripts', 'oas_scripts');

// Add options ACF
if( function_exists('acf_add_options_page') ) {
	acf_add_options_page();
}

// Add menus
function amtele_menu() {
  register_nav_menu('amtele_menu',__( 'Amtele menu' ));
}
add_action( 'init', 'amtele_menu' );

function amtele_menu_footer() {
  register_nav_menu('amtele_menu_footer',__( 'Amtele menu footer' ));
}
add_action( 'init', 'amtele_menu_footer' );

// Custom block categories
function amtele_blocks( $categories, $post ) {
	return array_merge(
		$categories,
		array(
			array(
				'slug' => 'amtele-blocks',
				'title' => 'Amtele blocks',
			),
		)
	);
}
add_filter( 'block_categories_all', 'amtele_blocks', 10, 2);

// Register ACF blocks
if (function_exists('acf_register_block_type')) {
  add_action('acf/init', 'register_acf_block_types');
}

function register_acf_block_types() {
  acf_register_block_type (
    array(
      'name' => 'hero',
      'title' => __('Jumbotron'),
      'description' => __('Hero section'),
      'render_template' => 'template_parts/blocks/hero.php',
      'category' => 'amtele-blocks',
      'keywords' => array('jumbotron', 'hero'),
      'mode' => 'edit'
    )
  );
  acf_register_block_type (
    array(
      'name' => 'gateway',
      'title' => __('Ingångar'),
      'description' => __('Gateway section'),
      'render_template' => 'template_parts/blocks/gateway.php',
      'category' => 'amtele-blocks',
      'keywords' => array('gateway'),
      'mode' => 'edit'
    )
  );
  acf_register_block_type (
    array(
      'name' => 'call_to_action',
      'title' => __('Uppmaning'),
      'description' => __('Call to action section'),
      'render_template' => 'template_parts/blocks/call-to-action.php',
      'category' => 'amtele-blocks',
      'keywords' => array('cta', 'uppmaning'),
      'mode' => 'edit'
    )
  );
  acf_register_block_type (
    array(
      'name' => 'image_text',
      'title' => __('Bild & text'),
      'description' => __('Image text section'),
      'render_template' => 'template_parts/blocks/image-text.php',
      'category' => 'amtele-blocks',
      'keywords' => array('text', 'image'),
      'mode' => 'edit'
    )
  );
  acf_register_block_type (
    array(
      'name' => 'employees',
      'title' => __('Anställda'),
      'description' => __('employees section'),
      'render_template' => 'template_parts/blocks/employees.php',
      'category' => 'amtele-blocks',
      'keywords' => array('personal', 'employees'),
      'mode' => 'edit'
    )
  );
  acf_register_block_type (
    array(
      'name' => 'available_positions',
      'title' => __('Lediga tjänster'),
      'description' => __('Available positions section'),
      'render_template' => 'template_parts/blocks/available-positions.php',
      'category' => 'amtele-blocks',
      'keywords' => array('personal', 'employees', 'positions'),
      'mode' => 'edit'
    )
  );
  acf_register_block_type (
    array(
      'name' => 'columns',
      'title' => __('Kolumner'),
      'description' => __('columns section'),
      'render_template' => 'template_parts/blocks/columns.php',
      'category' => 'amtele-blocks',
      'keywords' => array('columns'),
      'mode' => 'edit'
    )
  );
  acf_register_block_type (
    array(
      'name' => 'suppliers',
      'title' => __('Leverantörer'),
      'description' => __('suppliers section'),
      'render_template' => 'template_parts/blocks/suppliers.php',
      'category' => 'amtele-blocks',
      'keywords' => array('Leverantörer', 'suppliers'),
      'mode' => 'edit'
    )
  );
  acf_register_block_type (
    array(
      'name' => 'gateway_products',
      'title' => __('Ingångar Produkter'),
      'description' => __('gateway products section'),
      'render_template' => 'template_parts/blocks/gateway-products.php',
      'category' => 'amtele-blocks',
      'keywords' => array('Ingång', 'Produkter'),
      'mode' => 'edit'
    )
  );
  acf_register_block_type (
    array(
      'name' => 'page_title',
      'title' => __('Sidtitel'),
      'description' => __('page title section'),
      'render_template' => 'template_parts/blocks/page-title.php',
      'category' => 'amtele-blocks',
      'keywords' => array('titel'),
      'mode' => 'edit'
    )
  );
  acf_register_block_type (
    array(
      'name' => 'text',
      'title' => __('Löpande text'),
      'description' => __('text section'),
      'render_template' => 'template_parts/blocks/text.php',
      'category' => 'amtele-blocks',
      'keywords' => array('WYSIWYG', 'Text'),
      'mode' => 'edit'
    )
  );
  acf_register_block_type (
    array(
      'name' => 'product_archive',
      'title' => __('Arkiv produkter'),
      'description' => __('products archive'),
      'render_template' => 'template_parts/blocks/products-archive.php',
      'category' => 'amtele-blocks',
      'keywords' => array('products', 'archive'),
      'mode' => 'edit'
    )
  );
}

// Register post thumb
add_theme_support( 'post-thumbnails' );

// Register custom post type
function staff_post_type() {
    register_post_type('staff',
        array(
            'labels'      => array(
                'name'          => __( 'Anställda' ),
                'singular_name' => __( 'Visa alla anställda' ),
            ),
            'public'      => true,
            'has_archive' => true,
            'publicly_queryable' => false,
            'rewrite'     => array( 'slug' => 'staff' ),
            'supports' => array(
              'title',
              'editor',
              'thumbnail',
              'title',
              'editor',
              'author',
              'excerpt',
              'trackbacks',
              'custom-fields',
              'comments',
              'revisions',
              'page-attributes',
              'post-formats'
            ),
            'show_in_rest' => true,
            'menu_icon' => 'dashicons-admin-users',
        )
    );
}
add_action('init', 'staff_post_type');

// Register custom post type
function available_positions() {
    register_post_type('available_positions',
        array(
            'labels'      => array(
                'name'          => __( 'Lediga tjänster' ),
                'singular_name' => __( 'Visa alla lediga tjänster' ),
            ),
            'public'      => true,
            'has_archive' => true,
            'rewrite'     => array( 'slug' => 'available-positions' ),
            'supports' => array(
              'title',
              'editor',
              'thumbnail',
              'title',
              'editor',
              'author',
              'excerpt',
              'trackbacks',
              'custom-fields',
              'comments',
              'revisions',
              'page-attributes',
              'post-formats'
            ),
            'show_in_rest' => true,
            'menu_icon' => 'dashicons-rest-api',
        )
    );
}
add_action('init', 'available_positions');

function supplier() {
    register_post_type('supplier',
        array(
            'labels'      => array(
                'name'          => __( 'Leverantörer' ),
                'singular_name' => __( 'Leverantör' ),
            ),
            'public'      => true,
            'has_archive' => true,
            'rewrite'     => array( 'slug' => 'supplier' ),
            'supports' => array(
              'title',
              'editor',
              'thumbnail',
              'title',
              'editor',
              'author',
              'excerpt',
              'trackbacks',
              'custom-fields',
              'comments',
              'revisions',
              'page-attributes',
              'post-formats'
            ),
            'show_in_rest' => true,
            'menu_icon' => 'dashicons-cart',
        )
    );
}
add_action('init', 'supplier');






// ****************************************************************************************
// Register custom post type

function products() {

    $lang = get_bloginfo('language');
    var_dump($lang);

    $products_slug = 'hej';
    var_dump($products_slug);

    if($lang === 'sv-SE') {
      $lang_rewrite_1 = 'produkter/%cat%';
    }

    if($lang === 'en-GB') {
      $lang_rewrite_1 = 'products/%cat%';
    }

    if($lang === 'fi') {
      $lang_rewrite_1 = 'tuotteet/%cat%';
    }
    

    register_post_type('products',
        array(
            'labels'      => array(
                'name'          => __( 'Produkter' ),
                'singular_name' => __( 'Produkt' ),
            ),
            'taxonomies' => array('produktkategori'),
            'public'      => true,
            'has_archive' => false,
            'rewrite' => array(
              'slug' => $lang_rewrite_1,
              // 'slug' => __( 'produkter', 'amtele' ),
              'with_front' => false
            ),
            'supports' => array(
              'title',
              'editor',
              'thumbnail',
              'title',
              'editor',
              'author',
              'excerpt',
              'trackbacks',
              'custom-fields',
              'comments',
              'revisions',
              'page-attributes',
              'post-formats',
              'categories'
            ),
            'show_in_rest' => true,
            'show_admin_column' => true,
            'menu_icon' => 'dashicons-cart',
        )
    );
}
add_action('init', 'products');

// %cat%
function vx_soon_training_post_link( $post_link, $id = 0 ) {
    $post = get_post( $id );
    if ( is_object( $post ) ) {
        $terms = wp_get_object_terms( $post->ID, 'produktkategori' );
        if ( $terms ) {
            return str_replace( '%cat%', $terms[0]->slug, $post_link );
        }
    }
    return $post_link;
}
add_filter( 'post_type_link', 'vx_soon_training_post_link', 1, 3 );

















// Singelsida page not found fix
function archive_rewrite_rules() {

    $lang = get_bloginfo('language');
    var_dump($lang);

    $products_slug = 'hej';
    var_dump($products_slug);

    if($lang === 'sv-SE') {
      add_rewrite_rule(
          '^produkter/(.*)/(.*)/?$',
          'index.php?post_type=products&name=$matches[2]',
          'top'
      );
    }

    if($lang === 'en-GB') {
      add_rewrite_rule(
          '^products/(.*)/(.*)/?$',
          'index.php?post_type=products&name=$matches[2]',
          'top'
      );
    }

    if($lang === 'fi') {
      add_rewrite_rule(
          '^tuotteet/(.*)/(.*)/?$',
          'index.php?post_type=products&name=$matches[2]',
          'top'
      );
    }


}
add_action( 'init', 'archive_rewrite_rules' );

// Register taxonomy
function products_categories() {
  
    $lang = get_bloginfo('language');
    var_dump($lang);

    $products_slug = 'hej';
    var_dump($products_slug);

    if($lang === 'sv-SE') {
      $lang_rewrite = 'produkter';
    }

    if($lang === 'en-GB') {
      $lang_rewrite = 'products';
    }

    if($lang === 'fi') {
      $lang_rewrite = 'tuotteet';
    }

    register_taxonomy(
      'produktkategori',
      'products',
      array(
        'hierarchical' => true,
        'label' => 'Produkter',
        'query_var' => true,
        'has_archive' => true,
        'show_ui' => true,
        'show_in_rest' => true,
        // Allow automatic creation of taxonomy columns on associated post-types table?
        'show_admin_column' => true,
        // Show in quick edit panel?
        'show_in_quick_edit' => true,
        'rewrite' => array(
          'slug' => $lang_rewrite,
          'with_front' => true
        )
      )
    );
}
add_action( 'init', 'products_categories');





















// Ajax filter query
add_action( 'wp_ajax_nopriv_filter', 'filter_ajax' );
add_action( 'wp_ajax_filter', 'filter_ajax' );

function filter_ajax() {

  $category = $_POST['category'];

  $args = array(
    'post_type' => 'post',
    'posts_per_page' => -1
  );

  if(isset($category)) {
    $args['category__in'] = array($category);
  }

  $query = new WP_Query($args);

  if($query->have_posts()) :
    while($query->have_posts()) : $query->the_post();
      $title_trimmed = wp_trim_words( get_the_title(), 6, '...' );
      ?>
        <div class="news-item">
          <a href="<?php the_permalink(); ?>">
            <div class="image-wrapper">
              <?php the_post_thumbnail(); ?>
            </div>
            <h4><?php echo esc_html( $title_trimmed ); ?></h4>
            <div class="button-wrapper with_arrow">
              <div class="btn secondary">
                <!-- Hårdkodat -->
                <span>Read more</span>
              </div>
            </div>
          </a>
        </div>
      <?php
    endwhile;
  endif;

  wp_reset_postdata();

  die();

}

// In your theme's functions.php
function customize_add_button_atts( $attributes ) {
  return array_merge( $attributes, array(
    'text' => 'Add Entry',
  ) );
}
add_filter( 'wpcf7_field_group_add_button_atts', 'customize_add_button_atts' );