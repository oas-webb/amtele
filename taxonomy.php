<?php
    get_header();
    global $post;
?>
<div class="wrapper taxonomy-wrapper">
    <div class="inner taxonomy-inner">
        <?php
            $current_category = get_queried_object();
            $term_text = get_field('category_text', $current_category);
            $taxonomy = get_field('taxonomi', 'options');
        ?>

        <div class="top-category-wrapper">
            <h1><?php echo $current_category->name; ?></h1>
            <p><?php echo $current_category->description; ?></p>
        </div>


        <main>
            <?php
            $display_products = get_field('display_products', $current_category);
            if( $display_products ): ?>
                <?php foreach( $display_products as $post ): 
                    //setup_postdata($post);
                    $relation = get_field( 'products', $display_products->ID );
                    $specific_supplier_link = get_field( 'specific_supplier_link', $display_products->ID );
                    $relation_logo = $relation['leverantorer_relation'];
                    ?>
                    <div class="product-item">
                        <div>
                            <?php the_post_thumbnail(); ?>
                            <h4><?php the_title() ;?></h4>
                            <?php the_excerpt(); ?>
                            <div class="button-wrapper">
                                <a href="<?php the_permalink() ?>" class="button red">
                                    <?php echo $taxonomy['knapptext']; ?>
                                </a>
                                <?php //echo $relation['specific_supplier_link']; ?>
                                
                                <?php if( !$relation['specific_supplier_link'] ): ?>

                                    <?php
                                        if($relation_logo) {
                                            foreach( $relation_logo as $relation_item ) {
                                                $relation_logo_src = get_field('supplier_post_type', $relation_item->ID);
                                                $relation_logo_url = get_field('supplier_post_type', $relation_item->ID); ?>

                                                <a href="<?php echo $relation_logo_src['url']; ?>">
                                                    <img class="supplier-logo-tax" src="<?php echo $relation_logo_src['image']['url']; ?>" alt="<?php echo $relation_logo_src['image']['title']; ?>">
                                                </a>



                                            <?php }
                                        }
                                        //var_dump($relation_logo);
                                    ?>
                                
                                <?php endif; ?>
                                
                                <?php if( $relation['specific_supplier_link'] ): ?>

                                    <a href="<?php echo $relation['specific_supplier_link']; ?>">
                                        <?php
                                            if($relation_logo) {
                                                foreach( $relation_logo as $relation_item ) {
                                                    $relation_logo_src = get_field('supplier_post_type', $relation_item->ID);
                                                    $relation_logo_url = get_field('supplier_post_type', $relation_item->ID); ?>

                                                        <img class="supplier-logo-tax" src="<?php echo $relation_logo_src['image']['url']; ?>" alt="<?php echo $relation_logo_src['image']['title']; ?>">
                                                        
                                                        <?php }
                                            }
                                            //var_dump($relation_logo);
                                        ?>
                                    </a>
                                
                                <?php endif; ?>

                            </div>
                        </div>
                    </div>
                <?php endforeach; ?>
                <?php 
                wp_reset_postdata(); ?>
            <?php endif; ?>
        </main>

        <div class="term-text-wrapper">
            <?php echo $term_text; ?>
        </div>
    </div>
</div>
<?php get_footer(); ?>