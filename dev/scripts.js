jQuery(document).ready(function($){

    $(".wpcf7-field-group-add").text("+");

    if($(window).width() > 767) {
    // change functionality for smaller screens
    var divs = $(".gfield");
    for(var i = 0; i < divs.length; i+=3) {
        divs.slice(i, i+3).wrapAll("<div class='new'></div>");
    }
    }


    console.log('Cloned yesbox');

    setTimeout(function() {
        $('#gform_confirmation_wrapper_1').addClass("out");
    }, 4000)

    // onPage animations
    setTimeout(function() {
        $('.title-wrapper h1').addClass("in");
    }, 700)
    
    // onPage animations
    setTimeout(function() {
        $('.single-wrapper .col').addClass("in");
    }, 700)

    // Burger toggle
    $(".burger-wrapper").click(function() {
        $(this).toggleClass("active");
        $('.mobile-nav').toggleClass("active");
    });

    // Wrap Form
    $('.gform_wrapper').wrap("<div class='wrapper'><div class='inner'></div></div>")
    // $('#field_1_6').wrap("<section></section>")
    // $('.one').wrapAll("<section></section>")

    $('.A1').toggleClass("in");
    $('.A2').toggleClass("in");
    $('.A').toggleClass("in");
    $('.M').toggleClass("in");
    $('.T').toggleClass("in");
    $('.E1').toggleClass("in");
    $('.L').toggleClass("in");
    $('.E2').toggleClass("in");

    // Check length
    var count_elements = $('.colum').length;
    console.log(count_elements);
    if(count_elements > 2) {
        $('.columns-wrapper').addClass("space-between");
    }
    if(count_elements < 3) {
        $('.columns-wrapper').addClass("center");
    }

});