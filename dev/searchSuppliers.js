// Suppliers search

//window.location.href;

const dataSupplierTemplate = document.querySelector("[data-supplier-template]")
const dataSupplierCardContainer = document.querySelector("[data-supplier-cards-container]")
const dataSearchSupplier = document.querySelector("[data-search-supplier]")
const dataBin = document.querySelector("[data-bin]")

var lang = jQuery('html').attr('lang')

let searchSuppliers = []

dataSearchSupplier.addEventListener("input", e => {
    const value = e.target.value.toLowerCase()

    searchSuppliers.forEach(supplier => {
        const isVisible = supplier.title.toLowerCase().includes(value)

        // try to remove instead
        supplier.element.classList.toggle("hide", !isVisible)
        supplier.element.classList.toggle("visible", isVisible)
        setTimeout(function(){
            supplier.element.classList.toggle("display-none", !isVisible)
        }, 350);
    })

    // Only run on our select menu
	if (event.target.id !== 'wizard') return;

    setTimeout(function(){
        let length_hidden = document.querySelectorAll('.hide').length;
        let length_visible = document.querySelectorAll('.visible').length;

        if(length_visible === 0) {
            var bin = document.getElementsByClassName('empty-bin-wrapper')
            dataBin.classList.add("posts-found")
        } else {
            dataBin.classList.remove("posts-found")
        }
    }, 500);


}, false)

const current_site = window.location.href;
console.log(current_site);

if(current_site.includes('amtele.local')) {
    fetch('http://amtele.local/wp-json/v2/supplier?per_page=100&orderby=title&order=asc')
    .then(res => res.json())
    .then(data => {
        searchSuppliers = data.map(supplier => {
            const card = dataSupplierTemplate.content.cloneNode(true).children[0]

            // Data
            const logo = card.querySelector("[data-logo]")
            const title = card.querySelector("[data-title]")
            const body = card.querySelector("[data-body]")
            const link = card.querySelector("[data-link]")

            // Actual data
            title.textContent = supplier.title.rendered
            body.textContent = supplier.acf.supplier_post_type.text
            logo.src = supplier.acf.supplier_post_type.image.url
            link.href = supplier.acf.supplier_post_type.url

            dataSupplierCardContainer.append(card);

            // Return
            return {
                title: supplier.title.rendered,
                body: supplier.acf.supplier_post_type.text,
                logo: supplier.acf.supplier_post_type.image.url,
                link: supplier.acf.supplier_post_type.url,
                element: card
            }
        })
    })
}

if(current_site.includes('localhost')) {
    fetch('http://amtele.local/wp/wp-json/v2/supplier?per_page=100&orderby=title&order=asc')
    .then(res => res.json())
    .then(data => {
        searchSuppliers = data.map(supplier => {
            const card = dataSupplierTemplate.content.cloneNode(true).children[0]

            // Data
            const logo = card.querySelector("[data-logo]")
            const title = card.querySelector("[data-title]")
            const body = card.querySelector("[data-body]")
            const link = card.querySelector("[data-link]")

            // Actual data
            title.textContent = supplier.title.rendered
            body.textContent = supplier.acf.supplier_post_type.text
            logo.src = supplier.acf.supplier_post_type.image.url
            link.href = supplier.acf.supplier_post_type.url

            dataSupplierCardContainer.append(card);

            // Return
            return {
                title: supplier.title.rendered,
                body: supplier.acf.supplier_post_type.text,
                logo: supplier.acf.supplier_post_type.image.url,
                link: supplier.acf.supplier_post_type.url,
                element: card
            }
        })
    })
}

if(current_site.includes('engineering')) {
	if(lang == 'en-GB'){
		var fetch_str = 'https://amteleengineering.com/wp-json/wp/v2/supplier?per_page=100&orderby=title&order=asc';
	} else if(lang == 'fi') {
        var fetch_str = 'https://amteleengineering.fi/wp-json/wp/v2/supplier?per_page=100&orderby=title&order=asc';
	} else {
        var fetch_str = 'https://amteleengineering.se/wp-json/wp/v2/supplier?per_page=100&orderby=title&order=asc'; 
        // var fetch_str = 'https://engineering.amtele.se/wp-json/wp/v2/supplier?per_page=100&orderby=title&order=asc';
	}
    fetch(fetch_str)
    .then(res => res.json())
    .then(data => {
        searchSuppliers = data.map(supplier => {
            const card = dataSupplierTemplate.content.cloneNode(true).children[0]

            // Data
            const logo = card.querySelector("[data-logo]")
            const title = card.querySelector("[data-title]")
            const body = card.querySelector("[data-body]")
            const link = card.querySelector("[data-link]")
			
			// Remove html tags
            const originalBody = body.textContent = supplier.acf.supplier_post_type.text;
            const regex = originalBody.replace(/<[^>]*>/g, '');

            // Actual data
            title.textContent = supplier.title.rendered
            body.textContent = regex
            logo.src = supplier.acf.supplier_post_type.image.url
            link.href = supplier.acf.supplier_post_type.url

            dataSupplierCardContainer.append(card);

            // Return
            return {
                title: supplier.title.rendered,
                body: supplier.acf.supplier_post_type.text,
                logo: supplier.acf.supplier_post_type.image.url,
                link: supplier.acf.supplier_post_type.url,
                element: card
            }
        })
    })
}

if(current_site.includes('communication')) {
	
    if(lang == 'en-GB'){
        var fetch_str = 'https://communication.amtele.se/wp/wp-json/wp/v2/supplier?per_page=100&orderby=title&order=asc';
	} else if(lang == 'fi') {
        var fetch_str = 'https://communication.amtele.se/wp/wp-json/wp/v2/supplier?per_page=100&orderby=title&order=asc';
	} else {
        var fetch_str = 'https://communication.amtele.se/wp/wp-json/wp/v2/supplier?per_page=100&orderby=title&order=asc'; 
	}

    fetch(fetch_str)
    .then(res => res.json())
    .then(data => {
        searchSuppliers = data.map(supplier => {
            const card = dataSupplierTemplate.content.cloneNode(true).children[0]

            // Data
            const logo = card.querySelector("[data-logo]")
            const title = card.querySelector("[data-title]")
            const body = card.querySelector("[data-body]")
            const link = card.querySelector("[data-link]")
			
			// Remove html tags
            const originalBody = body.textContent = supplier.acf.supplier_post_type.text;
            const regex = originalBody.replace(/<[^>]*>/g, '');

            // Actual data
            title.textContent = supplier.title.rendered
            body.textContent = regex
            logo.src = supplier.acf.supplier_post_type.image.url
            link.href = supplier.acf.supplier_post_type.url

            dataSupplierCardContainer.append(card);

            // Return
            return {
                title: supplier.title.rendered,
                body: supplier.acf.supplier_post_type.text,
                logo: supplier.acf.supplier_post_type.image.url,
                link: supplier.acf.supplier_post_type.url,
                element: card
            }
        })
    })
}