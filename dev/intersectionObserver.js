// lazy load and transition all images
const images = document.querySelectorAll("[data-src]");
function preloadImage(img) {
    const src = img.getAttribute("data-src");
    img.src = src;
    console.log('image loaded');
    if(!src) {
        return;
    } else {
        img.src = src;
    }
}
const imgOptions = {}
const imgObserver = new IntersectionObserver((entries, imgObserver) => {
    entries.forEach(entry => {
        if(!entry.isIntersecting) {
            return;
        } else {
            entry.target.classList.toggle("show", entry.isIntersecting)
            imgObserver.unobserve(entry.target);
            preloadImage(entry.target);
        }
    })
}, {
    root: null,
    rootMargin: '0px',
    threshold: 0
})
images.forEach(image => {
    imgObserver.observe(image);
})

// Fade in sections
const sections = document.querySelectorAll(".block");
const observer = new IntersectionObserver(function(entries, observer) {
    entries.forEach(entry => {
        if(!entry.isIntersecting) {
            return;
        } {
            entry.target.classList.toggle("show", entry.isIntersecting)
        }
    })
}, {
    // threshold: 0.5,
    rootMargin: "-50px",
});
sections.forEach(section => {
    observer.observe(section);
})