<?php
    $gateway_products = get_field('gateway_products');
    $products = $gateway_products['products'];
    // var_dump($gateway_products);
?>

<div class="wrapper block">
    <div class="inner">
        <ul class="gateway-products-wrapper">
            <h2><?php echo $gateway_products['heading']; ?></h2>
            <?php foreach( $products as $product ): ?>
                <li class="product-item">
                    <a href="<?php echo $product['link']['url']; ?>">
                        <div class="image-wrapper <?php echo $product['background_color']; ?>">
                            <img src="<?php echo $product['image']['url']; ?>" alt="<?php echo $product['image']['alt']; ?>">
                        </div>
                        <h4><?php echo $product['link']['title']; ?></h4>
                    </a>
                </li>
            <?php endforeach; ?>
        </ul>
    </div>
</div>