<?php
    $args = array(
        'post_type' => 'staff',
        'posts_per_page' => -1,
        'orderby' => 'menu_order', 
        'order' => 'ASC', 
        
    );
?>
<div class="wrapper">
    <div class="inner">
        <div class="staff-wrapper">
            <?php $the_query = new WP_Query( $args ); ?>
            <?php if ( $the_query->have_posts() ) : ?>
                <?php while ( $the_query->have_posts() ) : $the_query->the_post(); ?>
                    <?php
                        $PostID = get_the_ID();
                        $info = get_field('personalinfo', $PostID);
                    ?>
                    <div class="staff-item">
                        <div class="staff-image">
                            <?php the_post_thumbnail(); ?>
                        </div>
                        <div class="staff-meta">
                            <?php
                                the_content();
                                echo $info;
                            ?>
                        </div>
                    </div>
                <?php endwhile; ?>
                <?php wp_reset_postdata(); ?>
            <?php endif; ?>
        </div>
    </div>
</div>