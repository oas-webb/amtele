<?php
    $archive_products = get_field('archive_products');
?>

<div class="wrapper page-wrapper">
    <div class="inner page-inner product-archive-wrapper">

            <ul class="scroll-section-nav">
                <?php foreach( $archive_products as $archive_product ): ?>
                    <li>
                        <a href="#<?php echo $archive_product['top']['icon']['title']; ?>">
                            <?php echo $archive_product['top']['heading']; ?>
                        </a>
                    </li>
                <?php endforeach; ?>
            </ul>

            <?php foreach( $archive_products as $archive_product ): ?>
                <section id="<?php echo $archive_product['top']['icon']['title']; ?>">
                    <div class="top-banner">
                        <div class="icon-wrapper">
                            <img src="<?php echo $archive_product['top']['icon']['url']; ?>" alt="">
                        </div>
                        <h3><?php echo $archive_product['top']['heading']; ?></h3>
                    </div>
                    <div class="term-wrapper">
                        <?php foreach( $archive_product['products_category'] as $term ) { ?>
                            <div class="term-group-wrapper" style="background: <?php echo $term['background_color']; ?>">
                                <div class="term-title-wrapper">
                                    <?php echo $term['heading']; ?>
                                </div>
                                <?php
                                    foreach( $term['links'] as $link ) {
                                        foreach( $link as $link_item ) { ?>
                                            <a href="<?php echo $link_item['url']; ?>" target="<?php echo $link_item['target']; ?>" class="link-item">
                                                <?php echo $link_item['title']; ?>
                                            </a>
                                        <?php }
                                    }
                                ?>
                            </div>
                        <?php } ?>

                    </div>
                </section>
            <?php endforeach; ?>

            <?php if ( have_posts() ) : ?>
                <?php while ( have_posts() ) : the_post(); ?>
                    <?php the_content(); ?>
                <?php endwhile; ?>
            <?php endif; ?>

    </div>
</div>
