<?php
    $gateway = get_field('gateway');
    $gateway_engineering = $gateway['gateway_engineering'];
    $gateway_communications = $gateway['gateway_communications'];
?>
<div class="wrapper block">
    <div class="inner">
        <div class="gateway-wrapper">
            <div class="gateway engineering">
                <div class="img-wrapper">
                    <img src="<?php echo $gateway_engineering['image']['sizes']['large']; ?>" alt="<?php echo $gateway_engineering['image']['alt']; ?>">
                </div>
                <h2><?php echo $gateway_engineering['heading']; ?></h2>
                <?php echo $gateway_engineering['text']; ?>
                <?php if($gateway_engineering['link']['title']): ?>
                    <div class="button-wrapper flex-start">
                        <a target="<?php echo $gateway_engineering['link']['target']; ?>" href="<?php echo $gateway_engineering['link']['url']; ?> " class="button red">
                            <?php echo $gateway_engineering['link']['title']; ?>
                        </a>
                    </div>
                <?php endif; ?>
            </div>
            <div class="gateway communications">
                <div class="img-wrapper">
                    <img src="<?php echo $gateway_communications['image']['sizes']['large']; ?>" alt="<?php echo $gateway_communications['image']['alt']; ?>">
                </div>
                <h2><?php echo $gateway_communications['heading']; ?></h2>
                <?php echo $gateway_communications['text']; ?>
                <?php if($gateway_communications['link']['title']): ?>
                    <div class="button-wrapper flex-start">
                        <a target="<?php echo $gateway_communications['link']['target']; ?>" href="<?php echo $gateway_communications['link']['url']; ?> " class="button red">
                            <?php echo $gateway_communications['link']['title']; ?>
                        </a>
                    </div>
                <?php endif; ?>
            </div>
        </div>
    </div>
</div>