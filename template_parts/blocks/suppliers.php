<?php
    $suppliers = get_field('suppliers');
?>

<div class="wrapper block">
    <div class="inner">
        <h2><?php echo $suppliers['heading']; ?></h2>
        <div class="content">
            <?php echo $suppliers['text']; ?>
        </div>

        <div class="search-wrapper">
            <label for="search"><?php echo $suppliers['search_label']; ?></label>
            <input id="wizard" type="search" placeholder="<?php echo $suppliers['search_placeholder']; ?>" id="search" data-search-supplier>
        </div>
        <div class="suppliers-cards" data-supplier-cards-container></div>
        <div data-bin class="empty-bin-wrapper">
            <h4><?php echo $suppliers['search_message']; ?></h4>
        </div>


        <template data-supplier-template>
            <div class="card">
                <div class="logo-wrapper">
                    <img alt="logo" src="" data-logo class="logo"/>
                </div>
                <h4 data-title class="title"></h4>
                <div data-body class="body"></div>
                <div class="link">
                    <a data-link target="_blank" href="/">
                        <?php echo $suppliers['button_text_supplier']; ?>
                    </a>
                </div>
            </div>
        </template>
    </div>
</div>

<!-- 578 -->