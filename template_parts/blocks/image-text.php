<?php
    $image_text = get_field('image_text');
    // var_dump($image_text['link']);
?>
<div class="wrapper block">
    <div class="inner">
        <div class="image-text-wrapper">
            <div class="col text <?php echo $image_text['inverted']; ?>">
                <?php echo $image_text['text']; ?>
                <?php if($image_text['link']['title']): ?>
                    <div class="button-wrapper flex-start">
                        <a target="<?php echo $image_text['link']['target']; ?>" href="<?php echo $image_text['link']['url']; ?>" class="button <?php echo $image_text['link_variant']; ?>">
                            <?php echo $image_text['link']['title']; ?>
                        </a>
                    </div>
                <?php endif; ?>
            </div>
            <div class="col image <?php echo $image_text['inverted']; ?>">
                <img src="<?php echo $image_text['image']['sizes']['large']; ?>" alt="<?php echo $image_text['image']['alt']; ?>">
            </div>
        </div>
    </div>
</div>