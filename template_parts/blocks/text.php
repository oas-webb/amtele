<?php
    $text = get_field('text');
?>

<div class="wrapper block">
    <div class="inner">
        <?php echo $text['text']; ?>
    </div>
</div>