<?php
    $columns = get_field('columns');
    // var_dump($columns);
?>
<div class="wrapper block">
    <div class="inner">
        <div class="columns-wrapper">
            <?php foreach( $columns['columns'] as $column ): ?>
                <div class="colum">
                    <img src="<?php echo $column['image']['url']; ?>" alt="<?php echo $column['image']['alt']; ?>">
                    <div class="column-content">
                        <?php echo $column['content']; ?>
                    </div>

                </div>
            <?php endforeach; ?>
        </div>
    </div>
</div>