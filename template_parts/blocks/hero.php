<?php
    $hero = get_field('hero');
    $hero_image = $hero['image']['sizes']['large'];
    $hero_image_alt = $hero['image']['alt'];
    $hero_filter = $hero['filter'];
?>
<div class="wrapper block">
    <div class="inner">
        <div class="hero-wrapper <?php echo $hero_filter; ?>">
            <div class="img-wrapper">
                <img src="<?php echo $hero_image; ?>" alt="<?php echo $hero_image_alt; ?>">
            </div>
            <div class="content-wrapper">
                <div class="content-inner">
                    
                    <h1 class="ml12"><?php echo $hero['heading']; ?></h1>
                    
                    <?php echo $hero['text']; ?>
                    <?php if($hero['link']['title']): ?>
                        <div class="button-wrapper">
                            <a target="<?php echo $hero['link']['target']; ?>" href="<?php echo $hero['link']['url']; ?>" class="button <?php echo $hero['link_variant']; ?>">
                                <?php echo $hero['link']['title']; ?>
                            </a>
                        </div>
                    <?php endif; ?>
                </div>
            </div>
        </div>
    </div>
</div>