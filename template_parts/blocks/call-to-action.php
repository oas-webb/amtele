<?php
    $call_to_action = get_field('call_to_action');
?>

<div style="background-color: <?php echo $call_to_action['background_color']; ?>" class="call-to-action-wrapper block">
    <?php if($call_to_action['background_image']): ?>
        <div class="background-image" style="background: url( <?php echo $call_to_action['background_image']['url']; ?> ) no-repeat center; background-size: cover;"></div>
    <?php endif; ?>
    <div class="wrapper">
        <div class="inner">
            <h2><?php echo $call_to_action['heading']; ?></h2>
            <?php echo $call_to_action['text']; ?>
            <?php if( $call_to_action['link']['title'] ): ?>
                <div class="button-wrapper">
                    <a target="<?php echo $call_to_action['link']['target']; ?>" href="<?php echo $call_to_action['link']['url']; ?>" class="button <?php echo $call_to_action['link_variant']; ?>">
                        <?php echo $call_to_action['link']['title']; ?>
                    </a>
                </div>
            <?php endif; ?>
        </div>
    </div>
</div>

