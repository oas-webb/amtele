<?php
    $args = array(
        'post_type' => 'available_positions',
        'posts_per_page' => -1
    );
?>
<div class="wrapper block">
    <div class="inner">
        <div class="available-positions-wrapper">
            <!-- Hårdkodat -->
            <h2>Lediga tjänster</h2>
            <?php
            $available_positions = get_field('available_positions');
            if( $available_positions ): ?>
                <ul class="available-positions-items-wrapper">
                <?php foreach( $available_positions as $featured_post ):
                    $permalink = get_permalink( $featured_post->ID );
                    $title = get_the_title( $featured_post->ID );
                    $position_form = get_field( 'position_form', $featured_post->ID );
                    $position_date = get_field( 'position_date', $featured_post->ID );
                    ?>
                    <li>
                        <h4><?php echo esc_html( $title ); ?></h4>
                        <h4><?php echo esc_html( $position_form ); ?></h4>
                        <h4>Tillträde <?php echo esc_html( $position_date ); ?></h4>
                    </li>
                <?php endforeach; ?>
                </ul>
            <?php endif; ?>
        </div>
    </div>
</div>