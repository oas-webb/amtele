<?php get_header(); ?>
<div class="wrapper page-wrapper">
    <div class="inner page-inner error">
        <?php
            $actual_link = "$_SERVER[REQUEST_URI]";
            $error_text = get_field('error_text','options');
            
            $button_1 = get_field('button_1','options');
            $button_2 = get_field('button_2','options');

        ?>
        <p><?php echo $actual_link; ?></p>
        <h1>404</h1>
        <h3><?php echo $error_text; ?></h3>
        <div class="button-wrapper">
            <a target="<?php echo $button_1['target']; ?>" href="<?php echo $button_1['url']; ?>" class="button black">
                <?php echo $button_1['title']; ?>
            </a>
            <a target="<?php echo $button_2['target']; ?>" href="<?php echo $button_2['url']; ?>" class="button brandColor">
                <?php echo $button_2['title']; ?>
            </a>
        </div>
    </div>
</div>
<?php get_footer(); ?>